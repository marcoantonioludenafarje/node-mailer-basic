"use strict";
require('dotenv').config();
const EmailTemplate = require('email-templates').EmailTemplate
const nodemailer = require("nodemailer");
const path = require('path')

let users = [
    {
        name: 'Hitsy',
        email: 'hitsychatbot@gmail.com'
    },
    {
        name: 'Monica',
        email: 'hitsychatbot@gmail.com'
    },
    {
        name: 'Giancarlo',
        email: 'hitsychatbot@gmail.com'
    },

]
function loadTemplate(templateName, contexts) {
    let template = new EmailTemplate(`./templates/${templateName}`)
    return Promise.all(contexts.map((context) => {
        return new Promise((resolve, reject) => {
            template.render(context, (err, result) => {
                if (err) reject(err);
                else resolve({
                    email: result,
                    context,
                });
            });
        });
    }));
}

loadTemplate('welcome', users).then( async (results) => {
    //console.log(JSON.stringify(results))
    for(var i =0;i<results.length;i++){
        var result = results[i].email
        console.log('Este es mi result',result)
        await sendMail(result.subject,result.text, result.html)
        console.log('Se envio a',result.subject)
    }

})

function sendMail(subject,text,html){

    return new Promise((resolve, reject) => {
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                type: 'OAuth2',
                user: 'marcoantonioludenafarje@gmail.com',
                pass: 'Mypass',
                clientId: process.env.ID_CLIENT,
                clientSecret: process.env.SECRET_CLIENT,
                refreshToken: process.env.REFRESH_TOKEN,
                accessToken: process.env.ACCESS_TOKEN
            }
        });
        let mailOptions = {
            from: '"Marco Bot" <marcoantonioludenafarje@gmail.com>', // sender address
            to: "hitsychatbot@gmail.com", // list of receivers
            subject, // Subject line
            text, // plain text body
            html // html body
        };
        transporter.sendMail(mailOptions, function (err, res) {
            if (err) reject(err);
            else resolve();
        })

    });


}